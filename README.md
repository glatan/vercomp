# vercomp

A Rust library for comparing versions. It supports some versioning styles(not only for X.Y.Z).

## Example

[example.rs](./examples/example.rs)

```rust
use vercomp::{Stage, Version};

fn main() {
    // Version contains number and stage.
    let version_1 = Version::from("1.2.3-Alpha");
    let version_2 = Version::from("1.2.3-Alpha");
    if version_1 == version_2 {
        println!("version_1 equals version_2.");
    }
    // if version_1 < version_2 {
    //     Compile error.
    //     Version can not use ">=", ">", "<=" and "<".
    // }

    // Version.number is the number of the version. It supports "==", "!=", ">=", ">", "<=" and "<".
    let version_3 = Version::from("1.2.3");
    let version_4 = Version::from("1-2-3");
    if version_3.number == version_4.number {
        println!("version_3.number equals version_4.number.");
    }
    let version_5 = Version::from("1/2/3");
    let version_6 = Version::from("1~2");
    if version_5.number > version_6.number {
        println!("version_5.number is greater than version_6.number.");
    }
    let version_7 = Version::from("1:2");
    let version_8 = Version::from("1;2;3");
    if version_7.number < version_8.number {
        println!("version_7.number is less than version_8.number.");
    }

    // Version.stage is the stage of the version. It only supports "==" and "!=".
    let version_9 = Version::from("1.2.3-Alpha");
    if version_9.stage == Stage::Alpha {
        println!("version_9 is an alpha version.");
    }
    let _version_10 = Version::from("1.2.3-Beta");
    let _version_11 = Version::from("1.2.3");
    // if _version_10.stage > _version_11.stage {
    //     Compile error.
    //     Version.stage can not use ">=", ">", "<=" and "<".
    // }
    // "CASE INSENSITIVE"
    // Stage::Alpha   <= Contain alpha
    // Stage::Beta    <= Contain beta
    // Stage::Dev     <= Contain dev
    // Stage::Nightly <= Contain nightly
    // Stage::Rc      <= Contain rc
    // Stage::Stable  <= others
}
```

## License

This project is licensed under either of

* Apache License, Version 2.0 ([LICENSE-APACHE](./LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license ([LICENSE-MIT](./LICENSE-MIT) or http://opensource.org/licenses/MIT)
