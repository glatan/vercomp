use regex::Regex;

#[derive(PartialEq, Debug)]
pub enum Stage {
    Alpha,
    Beta,
    Development,
    Nightly,
    RC,
    Stable,
}

impl Stage {
    #[deprecated(since = "0.5.0", note = "superseded by `from`")]
    pub fn new(input: &str) -> Self {
        Self::from(input)
    }
}

impl From<&str> for Stage {
    fn from(input: &str) -> Self {
        // Alpha
        let alpha = Regex::new(r"(?i:alpha)").unwrap();
        if alpha.is_match(input) {
            return Self::Alpha;
        }
        // Beta
        let beta = Regex::new(r"(?i:beta)").unwrap();
        if beta.is_match(input) {
            return Self::Beta;
        }
        // Development
        let dev = Regex::new(r"(?i:dev)").unwrap();
        if dev.is_match(input) {
            return Self::Development;
        }
        // Nightly
        let nightly = Regex::new(r"(?i:nightly)").unwrap();
        if nightly.is_match(input) {
            return Self::Nightly;
        }
        // RC
        let rc = Regex::new(r"(?i:rc)").unwrap();
        if rc.is_match(input) {
            return Self::RC;
        }
        // Stable
        Self::Stable
    }
}

#[cfg(test)]
mod tests {
    use crate::stage::Stage;
    #[test]
    fn alpha() {
        let stage = Stage::from("1.2.3-alpha");
        assert_eq!(stage, Stage::Alpha);
    }
    #[test]
    fn beta() {
        let stage = Stage::from("1.2.3 Beta");
        assert_eq!(stage, Stage::Beta);
    }
    #[test]
    fn dev() {
        let stage = Stage::from("1.2.3.deV");
        assert_eq!(stage, Stage::Development);
    }
    #[test]
    fn nightly() {
        let stage = Stage::from("1.2.3-NightlY");
        assert_eq!(stage, Stage::Nightly);
    }
    #[test]
    fn rc() {
        let stage = Stage::from("1.2.3-RC1");
        assert_eq!(stage, Stage::RC);
    }
}
