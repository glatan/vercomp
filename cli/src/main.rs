use std::env;

use clap::{App, Arg, ArgGroup};
use vercomp::Version;

pub fn compare(operator: &str, version_1: Version, version_2: Version) -> bool {
    match operator {
        "eq" => {
            if version_1.number == version_2.number {
                return true;
            }
            false
        }
        "ne" => {
            if version_1.number != version_2.number {
                return true;
            }
            false
        }
        "ge" => {
            if version_1.number >= version_2.number {
                return true;
            }
            false
        }
        "gt" => {
            if version_1.number > version_2.number {
                return true;
            }
            false
        }
        "le" => {
            if version_1.number <= version_2.number {
                return true;
            }
            false
        }
        "lt" => {
            if version_1.number < version_2.number {
                return true;
            }
            false
        }
        _ => panic!("Failed to compare versions"),
    }
}

fn main() {
    let operators = &["eq", "ne", "ge", "gt", "le", "lt"];
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(Arg::with_name("version").required(true).index(1))
        .args_from_usage(
            "
            --eq [version]
            --ne [version]
            --ge [version]
            --gt [version]
            --le [version]
            --lt [version]
        ",
        )
        .group(ArgGroup::with_name("mode").args(operators).required(true))
        .get_matches();
    let operator = {
        let mut operator = String::new();
        for op in operators {
            if matches.is_present(op) {
                operator = (*op).to_string();
            }
        }
        operator
    };
    let version_1: Version = Version::from(matches.value_of("version").unwrap());
    let version_2: Version = Version::from(matches.value_of(&operator).unwrap());

    if compare(&operator, version_1, version_2) {
        println!("True");
    } else {
        println!("False");
    }
}

#[cfg(test)]
mod tests {
    use super::compare;
    use vercomp::Version;
    #[test]
    fn eq() {
        let operator = "eq";
        let version_1 = Version::from("1.2.3");
        let version_2 = Version::from("1.2.3");
        assert_eq!(compare(operator, version_1, version_2), true);
    }
    #[test]
    fn ne() {
        let operator = "ne";
        let version_1 = Version::from("1.2");
        let version_2 = Version::from("1.2.3");
        assert_eq!(compare(operator, version_1, version_2), true);
    }
    #[test]
    fn ge() {
        let operator = "ge";
        let version_1 = Version::from("1.2.3");
        let version_2 = Version::from("1.2.3");
        assert_eq!(compare(operator, version_1, version_2), true);
    }
    #[test]
    fn gt() {
        let operator = "gt";
        let version_1 = Version::from("1.2.3");
        let version_2 = Version::from("1.2");
        assert_eq!(compare(operator, version_1, version_2), true);
    }
    #[test]
    fn le() {
        let operator = "le";
        let version_1 = Version::from("1.2.3");
        let version_2 = Version::from("1.2.3");
        assert_eq!(compare(operator, version_1, version_2), true);
    }
    #[test]
    fn lt() {
        let operator = "lt";
        let version_1 = Version::from("1.2");
        let version_2 = Version::from("1.2.3");
        assert_eq!(compare(operator, version_1, version_2), true);
    }
}
