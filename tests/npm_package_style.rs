use vercomp::{Stage, Version};

use std::cmp::Ordering;

// Name: npm
// Repository: https://github.com/npm/cli

#[test]
fn eq() {
    let version_1 = Version::from("6.13.6");
    let version_2 = Version::from("6.13.6");
    assert_eq!(version_1.number.cmp(&version_2.number), Ordering::Equal);
}

#[test]
fn gt() {
    let version_1 = Version::from("6.13.6");
    let version_2 = Version::from("6.13.4");
    assert_eq!(version_1.number.cmp(&version_2.number), Ordering::Greater);
}

#[test]
fn lt() {
    let version_1 = Version::from("6.13.4");
    let version_2 = Version::from("6.13.6");
    assert_eq!(version_1.number.cmp(&version_2.number), Ordering::Less);
}

#[test]
fn stable() {
    let version = Version::from("6.13.6");
    assert_eq!(version.stage, Stage::Stable);
}
