use vercomp::{Stage, Version};

use std::cmp::Ordering;

// Name: Zola
// Repository: https://github.com/getzola/zola/

#[test]
fn eq() {
    let version_1 = Version::from("v0.9.0");
    let version_2 = Version::from("v0.9.0");
    assert_eq!(version_1.number.cmp(&version_2.number), Ordering::Equal);
}

#[test]
fn gt() {
    let version_1 = Version::from("v0.9.0");
    let version_2 = Version::from("v0.8.0");
    assert_eq!(version_1.number.cmp(&version_2.number), Ordering::Greater);
}

#[test]
fn lt() {
    let version_1 = Version::from("v0.8.0");
    let version_2 = Version::from("v0.9.0");
    assert_eq!(version_1.number.cmp(&version_2.number), Ordering::Less);
}

#[test]
fn stable() {
    let version = Version::from("v0.9.0");
    assert_eq!(version.stage, Stage::Stable);
}
